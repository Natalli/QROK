$(document).ready(function () {

    stickyHeader();
    chart();
    if ($(window).outerWidth() > 767) {
        animation();
    } else {
        slide();
        mobileMenu();
    }

    $(window).resize(function () {
        mobileMenu();
        animation();
        slide();
    });


});


/*fixed header*/
function stickyHeader() {
    $(window).on('scroll', function () {
        var windowScroll = $(window).scrollTop();
        if (windowScroll > 0) {
            $('header').addClass('sticky');
        }
        else {
            $('header').removeClass('sticky');
        }
    });
}

function animation() {
    /*animation*/
    $('.anLeft').addClass('hidden').viewportChecker({
        classToAdd: 'visible animated fadeInLeftBig',
        offset: 100
    });
    $('.anRight').addClass('hidden').viewportChecker({
        classToAdd: 'visible animated fadeInRightBig',
        offset: 100
    });
    $('.anUp').addClass('hidden').viewportChecker({
        classToAdd: 'visible animated fadeInUp',
        offset: 100
    });
}

function slide() {
    $('.footer-wrap .item .ttl').on('click', function () {
        $(this).parent('.item').find('ul').slideToggle()
    })
}

function mobileMenu() {
    $('.nav-trigger').on('click', function () {
        if ($('body').hasClass('show-nav')) {
            $('body').removeClass('show-nav');
        }
        else {
            $('body').addClass('show-nav');
        }
    });
}

function chart() {
    var ctx = document.getElementById("myChart").getContext('2d');
    ctx.height = 320;
    var myLineChart = new Chart(ctx, {

        type: 'line',
        data: {
            labels: ["28 February 2017", "11 May 2017", "8 August 2017", "21 November 2017", "15 February 2018", "21 May 2018"],
            datasets: [{
                borderColor: 'rgb(0, 105, 212)',
                data: [0, 5000, 15000, 10000]
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            scales: {

                yAxes: [{
                    position: 'right',
                    ticks: {
                        suggestedMax: 15000,
                        suggestedMin: 0,
                        stepSize: 5000,
                        fontSize: 12,
                        fontColor: 'rgb(68,68,68)',
                        fontFamily: 'Rubik Regular'
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 12,
                        fontColor: 'rgb(68,68,68)',
                        fontFamily: 'Rubik Regular'
                    }
                }]
            }
        }
    });
}